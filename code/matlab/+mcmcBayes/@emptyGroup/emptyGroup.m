classdef emptyGroup < mcmcBayes.model    
%mcmcBayes.emptyGroup is a base class for a new model group.  It is a sub class of mcmcBayes.model and mcmcBayes.simulation.
% The currently implemented models, listed hierarchically, are:
%  * mcmcBayes.simulation.model.emptyGroup.emptyModel
    properties (SetAccess=public, GetAccess=public)
        predictionsNSamples=mcmcBayes.emptyGroup.getDefaultNSamples();%Default number of samples to run in calls to simPredictions()
    end

    methods (Abstract, Static)        

    end
    
    methods (Abstract)

    end

    methods (Static)
        function test()
            display('Hello!');
        end
                
        function [predictionsNSamples]=getDefaultNSamples()
            predictionsNSamples=50000;
        end        
        
        function [names]=getDefaultDataVariableNames(dataCategory)
            error('Need to complete this.');%See mcmcBayes.regression for an example
            switch dataCategory
                case mcmcBayes.t_dataCategory.independent
                    names={'UPDATEME'};
                case mcmcBayes.t_dataCategory.dependent
                    names={'UPDATEME'};
                otherwise
                    error('dataCategory must be mcmcbayes.t_dataCategory.');
            end
        end        
        
        function [loglikelihood]=computeLogLikelihood(type, vars, data)
        % [loglikelihood]=computeLogLikelihood(type, vars, data)
        %   Computes the loglikelihood of the data given the model and parameter values.  This is used in the marginal likelihood computation.
        %   Input: 
        %     type - Specifies the model to use for computing the likelihood (e.g., mcmcBayes.emptyModel)
        %     vars - Array of model variable objects 
        %     data - Model data object that contains the independent and dependent variables
        %   Output:
        %     loglikelihood - Computed loglikelihood (vector)
            error('Need to complete this.');%See mcmcBayes.regression for an example
        end

        function [dependentVariables]=generateTestData(type, predictionsNSamples, data, varargin)
            dependentVariables=generateTestData@mcmcBayes.model(type, predictionsNSamples, data, varargin{:});
        end
        
        function [depVarsValuesHat, retYhat]=simPredictions(type, predictionsNSamples, predictionsToCapture, hingeThreshold, data, varargin)
        % [dependentVariablesHat]=simPredictions(type, predictionsNSamples, predictionsToCapture, hingeThreshold, data, varargin)
        %   Function to simulate predictions using one of the regression models (it is static so we can simulate data without instantiating a class)
        %   Input:
        %     type - Specifies the model to use for generating the predictions (e.g., mcmcBayes.emptyModel)
        %     predictionsNSamples - Number of samples to run when generating the predictions
        %     predictionsToCapture - Array of mcmcBayes.t_dataResult that specifies which thetahat to generate predictions for
        %     hingeThreshold - Specifies the hinge threshold for determining the thetaHat edge cases for generating dependentVariablesHat.{hingelo/hingehi}
        %     data - Model data object that contains the independent variables and descriptions for the dependent variables
        %     varargin - Cell array with the values to use for the model variables when predicting with the model (i.e., these are the thetaHat)
        %   Output:
        %     depVarsValuesHat - Struct with elements for each of the mcmcBayes.t_dataResult types specified in predictionsToCapture 
        %     (e.g., the mean and the {hingelo/hingehi} values from the histogram of the predicted values over predictionsNSamples samples)         
        %     retYhat - Array of predictions
            error('Need to complete this.');%See mcmcBayes.regression for an example
        end
    end
    
    methods 
        function obj=emptyGroup(settype, setsubType, setname, setdescription, setauthor, setreference, setprefix)
        % obj=emptyGroup(settype, setsubType, setname, setdescription, setauthor, setreference, setprefix)
        %   The constructor for a new group.
        % Input:
        %   settype - Model type
        %   setsubType - Model subtype
        %   setname - Name for the model
        %   setdescription - Description for the model
        %   setauthor - Author for the model
        %   setreference - Reference for the model
        %   setprefix - Filename prefix to use for creating simulation artifacts.
        % Output:
        %   obj Constructed emptyGroup object
            if nargin == 0
                superargs={};
            else
                superargs{1}=settype;
                superargs{2}=setsubType;
                superargs{3}=setname;
                superargs{4}=setdescription;
                superargs{5}=setauthor;
                superargs{6}=setreference;
                superargs{7}=setprefix;                                
            end

            obj=obj@mcmcBayes.model(superargs{:});           
        end
                
        function displayPredictions(obj, tempID)
        % displayPredictions(obj, tempid)
        %   Displays the values (in the console) for the already generated model predictions in either obj.priorPredictedData or obj.posteriorPredictedData.
        %   Input:
        %     obj - emptyGroup object
        %     tempID - power-posterior index of the predictions to display
            error('Need to complete this.');%See mcmcBayes.regression for an example
        end
        
        function plotPredictions(obj, chainID, tempID)
        % plotPredictions(obj, chainID, tempID)
        %   Plots the values for the already generated model predictions in either obj.priorPredictedData or obj.posteriorPredictedData.
        %   Input:
        %     chainID - MCMC chain index for the session details (filename prefix)
        %     tempID - power-posterior index of the predictions to plot
            error('Need to complete this.');%See mcmcBayes.regression for an example  
        end                
    end

    methods (Access=protected)
        function [status]=checkDataCompatibility(obj, datatype)
        % [status]=checkDataCompatibility(obj, datatype)
        %   Checks the compatibility of the specified datatype in obj's data objects with the model group
        %   Input:
        %     obj - 
        %     datatype - 
        %   Output:
        %     status - 
            switch datatype
                case mcmcBayes.t_data.priordata
                	status=obj.priorData.checkDataForInfNanValues();
                case mcmcBayes.t_data.priorbasedpredictions
                	status=obj.priorPredictedData.checkDataForInfNanValues();
                case mcmcBayes.t_data.posteriordata
                	status=obj.posteriorData.checkDataForInfNanValues();
                case mcmcBayes.t_data.posteriorbasedpredictions
                	status=obj.posteriorPredictedData.checkDataForInfNanValues();
            end
        end
    end
    
    methods %getter/setter functions       

    end
end
