classdef emptyModel < mcmcBayes.emptyGroup
% Empty model.  Copy to a new directory and rename.  Then, implement the functions below.
% Model subtype alternatives:
%   'a' - ??? specify the priors for all the model variables
    methods(Access = public, Static)
        function test()
            display('Hello!');
        end
        
        function [retData]=transformDataPre(vars, data)
        % [retData]=transformDataPre(vars, data)
        %   Transforms the data prior to the simulation.
        %   Input:
        %     vars - Array of model variable objects
        %     data - Model data object that contains the independent and dependent variables
        %   Output
        %     retData - Transformed model data object
            warning('Need to complete this (or remove warning if no special transforms)');
            retData=data; %If no special transforms for this model, just return data (everything handled by mcmcBayes.data.constructData())
        end        
        
        function [solverInits]=determineSolverInits(vars, data)
        % [solverInits]=determineSolverInits(vars, data)
        %   Estimates starting points for the solvers used to determine model variable initial values.
        %   Input:
        %     vars - Array of model variable objects
        %     data - Model data object that contains the independent and dependent variables
        %   Output
        %     solverInits - Cell array of model variable initial values to pass to solver (due to some models having different sized variables)
            error('Need to complete this.');%See mcmcBayes.linear for an example
        end        

        %When the data source is not mcmcBayes.t_dataSource.None or mcmcBayes.t_dataSource.Preset, you will need to implement functions for any 
        %mcmcBayes.t_variablePointEstimateSolver functions supported by this model.  Examples for MLE and LS are below.
        function [thetahat]=SolverMeanMLE(runPowerPosteriorMCMCSequence, subtype, vars, data)            
        % [thetahat]=mlefitModel(runPowerPosteriorMCMCSequence, subtype, vars, data)
        %   Wrapper function to call MATLAB's mle routine that estimates the model variable point estimates.  The structure
        %   of MATLAB's mle routine requires globals to pass the independent variables, phi, and any values for variables that
        %   are assumed as a constant (thus, are not being estimated by the mle).  Will also need to update computeLogLikelihood_alt() below.
        %   Input:
        %     runPowerPosteriorMCMCSequence - Boolean that specifies either running the power-posterior sequence, or not running
        %     subtype - Specifies the model subtype
        %     vars - Array of model variable objects 
        %     data - Model data object that contains the independent and dependent variables
        %   Output:
        %     thetahat - Cell array of model variable point estimates values computed (cell array due to some models having different sized variables)
            error('Need to complete this.');%See mcmcBayes.linear for an example
        end

        function [thetahat]=SolverMeanLS(runPowerPosteriorMCMCSequence, subtype, vars, data)
        % [thetahat]=lsfitModel(independentVariables, modelVariables, dependentVariables)
        %   Wrapper function to call MATLAB's lsqcurvefit routine that estimates the model variable point estimates.  
        %   Input:
        %     runPowerPosteriorMCMCSequence - Boolean that specifies either running the power-posterior sequence, or not running
        %     subtype - Specifies the model subtype
        %     vars - Array of model variable objects 
        %     data - Model data object that contains the independent and dependent variables
        %   Output:
        %     thetahat - Cell array of model variable point estimates values computed (cell array due to some models having different sized variables)
            error('Need to complete this.');%See mcmcBayes.linear for an example
        end                                               
    end

    methods(Access = private, Static)               
        function [loglikelihood]=computeLogLikelihood_alt(data,phi,x,varargin)
        % [loglikelihood]=computeLogLikelihood_alt(data,phi,x,varargin)
        %   Function that computes the model's log-likelihood of the data given the parameters.  It is used by MATLAB's mle function.
        %   It was necessary to utilize a global variable for passing the independent variables and phi.
        %   Input:
        %     data - Model dependent data used in computing the log-likelihood
        %     phi - Power-posterior value for the simulation
        %     x - Independent data variables are also passed in (may need more than one depending on your model)
        %     varargin - Point estimates for the model variables used in computing the log-likelihood
        %   Output:
        %     loglikelihood - Computed log-likelihood
            error('Need to complete this.');%See mcmcBayes.linear for an example
        end        
    end
    
    methods 
        function obj=emptyModel(setsimulationStruct, setprefix)
        % emptyModel(setsimulationStruct, setprefix)
        %   Constructor for the emptyModel class
        %   Input:
        %     setsimulationStruct - Structure that contains simulation settings read in from the *_simulationLUT.xml file.
        %     setprefix - Filename prefix to use for creating simulation artifacts.
        %   Output:
        %     obj - instantiated emptyModel object        
            warning('Need to complete this for new model.');%See mcmcBayes.linear for an example
            if nargin == 0
                superargs={};
            else
                switch setsimulationStruct.subType
                    case 'a'
                        %disp('emptyModel model is using model subtype a (prior summary goes here for all the model variables)');
                    otherwise
                        error('Unsupported model subtype');
                end
                superargs{1}='emptyModel';
                superargs{2}=setsimulationStruct.subType;
                superargs{3}='emptyModel of emptyGroup';
                superargs{4}=strcat('Empty model for creating new ones.');
                superargs{5}='Model reference goes here';
                superargs{6}='Your contact information goes here';
                superargs{7}=setprefix;                
            end
            
            obj=obj@mcmcBayes.emptyGroup(superargs{:});%call the parent constructor
            if nargin>0 %i am not sure why, but it is necessary to set these when loading
                obj.predictionsNSamples=mcmcBayes.emptyGroup.getDefaultNSamples();
            end
        end
        
        function [retObj]=transformVarsPre(obj, chainID, tempID)
        % [retObj]=transformVarsPre(obj, chainID, tempID)
        %   This function adjusts the model variable hyperparameters prior to a simulation.  If tempID==0, needs to be performed on all 
        %   obj.priorVariables for chainID.  Otherwise, needs to be performed on all obj.posteriorVariables for the specified {chainID, tempID} set.
        %   Input:
        %     obj - original emptyModel object
        %     chainID - MCMC chain index in the variables array to use
        %     tempID - power-posterior index in the variables array to use
        %   Output:
        %     retObj - updated emptyModel object
            warning('Need to complete this for new model.');%See mcmcBayes.linear for an example
            retObj=obj;%If no special transforms for this model, just return object
        end        
        
        function [retObj]=transformVarsPost(obj, chainID, tempID)
        % [retObj]=transformVarsPost(obj, chainID, tempID)
        %   This function adjusts the model variable hyperparameters after a simulation.  If tempID==0, needs to be performed on all 
        %   obj.priorVariables for chainID.  Otherwise, needs to be performed on all obj.posteriorVariables for the specified {chainID, tempID} set.
        %   Input:
        %     obj - original emptyModel object
        %     chainID - MCMC chain index in the variables array to use
        %     tempID - power-posterior index in the variables array to use
        %   Output:
        %     retObj - updated emptyModel object
            warning('Need to complete this for new model.');%See mcmcBayes.linear for an example
            retObj=obj;%If no special transforms for this model, just return object
        end
    end
end
        